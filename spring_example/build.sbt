name := "spring_example"

version := "0.1"

scalaVersion := "2.12.6"

libraryDependencies += "org.springframework.boot" % "spring-boot-starter-web" % "2.1.0.RELEASE"
libraryDependencies ++= Seq(
"io.circe" %% "circe-core",
"io.circe" %% "circe-generic",
"io.circe" %% "circe-parser"
).map(_ % "0.10.1")

libraryDependencies += "org.springframework.boot" % "spring-boot-starter-test" % "2.1.0.RELEASE" % Test
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.0-SNAP10" % Test
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.0" % Test
libraryDependencies += "org.scalamock" %% "scalamock" % "4.1.0" % Test

addCompilerPlugin(
  "org.scalamacros" % "paradise" % "2.1.1" cross CrossVersion.full
)

mainClass in(Compile, run) := Some("org.sample.Application")