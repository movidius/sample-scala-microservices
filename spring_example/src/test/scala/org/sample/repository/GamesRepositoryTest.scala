package org.sample.repository

import org.sample.exceptions.{BadRequest, NotFound}
import org.sample.models.Game
import org.scalatest.{BeforeAndAfterEach, FunSuite}

class GamesRepositoryTest extends FunSuite with BeforeAndAfterEach {
  private val gamesRepository = new GamesRepository

  private val witcher = Game("witcher3", 2015, 40)
  private val invalidGame = Game("_!@#$%^&*()-=+", 2018, 50)

  override def afterEach(): Unit = gamesRepository retrieveGames() clear()

  test("retrieveGames returns empty list on new repository") {
    assert(gamesRepository retrieveGames() isEmpty)
  }

  test("Adding an invalid game throws BadRequest with appropriate message") {
    val e = intercept[BadRequest] {
      gamesRepository addNewGame invalidGame
    }

    assert(e.getMessage equals "Invalid Id")
  }

  test("Adding an invalid game does not change the list") {
    val games = gamesRepository retrieveGames()

    intercept[BadRequest] {
      gamesRepository addNewGame invalidGame
    }

    val postAddGames = gamesRepository retrieveGames()

    assert(games equals postAddGames)
  }

  test("Adding a valid, already present game throws BadRequest with appropriate message") {
    gamesRepository addNewGame witcher

    val e = intercept[BadRequest] {
      gamesRepository addNewGame witcher
    }

    assert(e.getMessage equals "Id already present. Try editing the object instead")

    //cleanup
    gamesRepository deleteGame witcher.name
  }

  test("Adding a valid game changes the list") {
    gamesRepository addNewGame witcher
    val games = gamesRepository retrieveGames()

    assert(games.size equals 1)
    assert(games("witcher3") equals witcher )
  }

  test("Adding a valid game returns its name") {
    assert(gamesRepository addNewGame witcher equals witcher.name)
  }

  test("DeleteGame of invalid id throws NotFound with appropriate message") {
    val e = intercept[NotFound] {
      gamesRepository deleteGame "witcher 3"
    }

    assert(e.getMessage equals "Id not present")
  }

  test("DeleteGame of valid id removes element") {
    val games = gamesRepository retrieveGames()

    gamesRepository addNewGame witcher
    gamesRepository deleteGame "witcher3"
    val newGames = gamesRepository retrieveGames()

    assert(newGames canEqual games)
  }
}
