package org.sample.controller

import io.circe.generic.auto._
import io.circe.syntax._
import io.circe._
import io.circe.parser._
import org.sample.exceptions.{BadRequest, NotFound}
import org.sample.models.Game
import org.sample.repository.GamesRepository
import org.sample.service.GamesService
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterEach, FeatureSpec, GivenWhenThen, Matchers}
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders

import scala.collection.immutable.TreeMap

class GamesItTest extends FeatureSpec with GivenWhenThen with Matchers with MockFactory with BeforeAndAfterEach {

  var gamesService: GamesService = new GamesService(gamesRepository = new GamesRepository)
  var gamesController: GamesController = new GamesController(gamesService)
  var mockMvc: MockMvc = MockMvcBuilders.standaloneSetup(gamesController).build()

  override def beforeEach(): Unit = gamesService.retrieveGames().clear()

  info("As a gamer")
  info("I want to be able to keep track of all my current games")
  info("So I know what to buy")
  info("And avoid duplicates")

  Feature("Retrieving games") {

    info("As a gamer")
    info("I want to be able to view my current games")

    Scenario("When there are no current games") {

      val emptyMap: TreeMap[String, Game] = TreeMap.empty
      val emptyMapAsString = emptyMap.asJson.toString()

      When("the first request to /games is sent")
      val response = mockMvc.perform(MockMvcRequestBuilders.get("/games"))
        .andReturn().getResponse

      val games = stringToTreeMapConverter(response.getContentAsString)

      Then("we get a response with an empty list in the body")
      response.getStatus shouldBe 200
      games shouldBe empty
    }

    Scenario("When there are 2 games") {

      Given("2 distinct valid games have been added")
      val witcher = Game("witcher3", 2015, 50)
      val darkSouls = Game("darkSouls", 2011, 45)
      gamesController addNewGame witcher
      gamesController addNewGame darkSouls

      When("a request to /games is sent")
      val response = mockMvc.perform(MockMvcRequestBuilders.get("/games"))
        .andReturn().getResponse

      val games = stringToTreeMapConverter(response.getContentAsString)

      Then("we get a response with a list of 2 games in the body")
      response.getStatus shouldBe 200
      games should (have size 2 and contain (witcher.name -> witcher) and contain (darkSouls.name -> darkSouls))
    }
  }

  Feature("Adding games") {

    Scenario("Adding a valid, already present game throws BadRequest with appropriate message") {
      Given("witcher is a valid game")
      val witcher = Game("witcher3", 2015, 50)

      When("a POST request to /games with witcher is made")
      gamesController addNewGame witcher
      val response = intercept[BadRequest] {
        gamesController addNewGame witcher
      }

      Then("an exception containing a self explanatory message is thrown")
      response.message shouldBe "Id already present. Try editing the object instead"
    }

    Scenario("Adding a valid game returns the id as a header and creates the resource") {

      Given("witcher is a valid game")
      val witcher = Game("witcher3", 2015, 50)

      When("a POST request to /games with witcher is made")
      val response = gamesController addNewGame witcher

      Then("a new resource is created")
      val gamesResponse = mockMvc.perform(MockMvcRequestBuilders.get("/games"))
        .andReturn().getResponse

      val games = stringToTreeMapConverter(gamesResponse.getContentAsString)
      games should (have size 1 and contain(witcher.name -> witcher))
    }
  }

  Feature("Updating games") {

    Scenario("Updating a non-existing game throws NotFound with the appropriate message") {
      Given("A valid game")
      val witcher = Game("witcher3", 2015, 50)

      When("a PUT request to /games/witcher3 is made")
      val response = intercept[NotFound] {
        gamesController editGame(witcher, witcher.name)
      }

      Then("NotFound exception is thrown with a self explanatory message")
      response.message shouldBe "Id not present"
    }

    Scenario("Editing a valid game successfully updates") {
      Given("A valid game")
      val witcher = Game("witcher2", 2013, 30)
      val witcherNew = Game("witcher3", 2015, 50)
      gamesController addNewGame witcher

      When("a PUT request to /games/witcher2 is made")
      gamesController editGame(witcherNew, witcher.name)

      Then("the resource is updated to witcher3")
      val response = mockMvc.perform(MockMvcRequestBuilders.get("/games"))
        .andReturn().getResponse

      val games = stringToTreeMapConverter(response.getContentAsString)
      response.getStatus shouldBe 200
      games should (have size 1 and contain (witcherNew.name -> witcherNew))
    }
  }

  Feature("Deleting games") {

    Scenario("Deleting a non-existing game throws NotFound with the appropriate message") {
      Given("A valid game")
      val witcher = Game("witcher3", 2015, 50)

      When("a DELETE request to /games/witcher3 is made")
      val response = intercept[NotFound] {
        gamesController deleteGame witcher.name
      }

      Then("NotFound exception is thrown with a self explanatory message")
      response.message shouldBe "Id not present"
    }

    Scenario("Deleting a valid game successfully removes it") {
      Given("A valid game")
      val witcher = Game("witcher3", 2015, 50)
      gamesController addNewGame witcher

      When("a PUT request to /games/witcher2 is made")
      gamesController deleteGame witcher.name

      Then("the resource is updated to witcher3")
      val response = mockMvc.perform(MockMvcRequestBuilders.get("/games"))
        .andReturn().getResponse

      val games = stringToTreeMapConverter(response.getContentAsString)
      response.getStatus shouldBe 200
      games should have size 0
    }
  }

  private def stringToTreeMapConverter(input: String): TreeMap[String, Game] = {
    val responseContents = parse(input).getOrElse(throw new ClassCastException)

    Decoder[TreeMap[String, Game]].decodeJson(responseContents).getOrElse(throw new ClassCastException)
  }
}
