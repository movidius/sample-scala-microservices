package org.sample.controller

import io.circe.generic.auto._
import io.circe.syntax._
import org.sample.models.Game
import org.scalatest.{FeatureSpec, GivenWhenThen, Matchers}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.{HttpEntity, HttpHeaders, HttpMethod}
import org.springframework.test.context.TestContextManager

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class GamesClientSideTest extends FeatureSpec with GivenWhenThen with Matchers {

  @Autowired
  var restTemplate: TestRestTemplate = _
  new TestContextManager(this.getClass).prepareTestInstance(this)

  info("Checking the way the information is displayed to the client is correct")

  Feature("Successful requests") {
    Scenario("Retrieving the games") {
      Given("A valid game")
      val witcher = Game("witcher3", 2015, 50)

      When("a GET request is made")
      val response = restTemplate.getForEntity("/games", classOf[String])

      Then("the response has http code 200")
      response.getStatusCodeValue shouldBe 200
    }

    Scenario("Adding a new game") {
      Given("A valid game")
      val witcher = Game("witcher3", 2015, 50)

      When("a POST request is made with the valid game")
      val headers = new HttpHeaders()
      headers add("Content-type", "application/json")
      val request = new HttpEntity(witcher.asJson.toString(), headers)

      val response = restTemplate.postForEntity("/games", request, classOf[String])

      Then("the response has http code 201 and the id of the new resource is in the headers")
      response.getStatusCodeValue shouldBe 201
      response.getHeaders.get("id") should (have size 1 and contain(witcher.name))
    }

    Scenario("Updating an existing game") {
      Given("An existing game and its new information")
      val witcher = Game("witcher2", 2013, 30)
      val witcherNew = Game("witcher3", 2015, 50)

      When("a PUT request is made")
      val headers = new HttpHeaders()
      headers add("Content-type", "application/json")
      val request = new HttpEntity(witcher.asJson.toString(), headers)

      val response = restTemplate.exchange("/games/witcher3", HttpMethod.PUT, request, classOf[String])

      Then("the response has http code 204")
      response.getStatusCodeValue shouldBe 204
    }
  }

  Feature("Failing requests") {
    Scenario("Creating an invalid game") {
      Given("An invalid game")
      val witcher = Game(" 2352352351(*&^%$#", -5, -1)

      When("a POST request is made")
      val headers = new HttpHeaders()
      headers add("Content-type", "application/json")
      val request = new HttpEntity(witcher.asJson.toString(), headers)

      val response = restTemplate.postForEntity("/games", request, classOf[String])

      Then("the response has http code 400 and the message explains the situation")
      response.getStatusCodeValue shouldBe 400
      response.getBody should include ("Invalid Id")
    }

    Scenario("Editing a game that doesn't exist") {
      When("A PUT request is made to update a game that does not exist")
      val headers = new HttpHeaders()
      headers add("Content-type", "application/json")
      val request = new HttpEntity(Game("doom", 1995, 10), headers)

      val response = restTemplate.exchange("/games/witcher3", HttpMethod.PUT, request, classOf[String])

      Then("the response has http code 404 and a message explaining the situation")
      response.getStatusCodeValue shouldBe 404
      response.getBody should include ("Id not present")
    }

    Scenario("Deleting a game that doesn't exist") {
      When("A DELETE request is made")
      val response = restTemplate.exchange("/games/witcher3", HttpMethod.DELETE, HttpEntity.EMPTY, classOf[String])

      Then("the response has http code 404 and a message explainig the situation")
      response.getStatusCodeValue shouldBe 404
      response.getBody should include ("Id not present")
    }
  }
}
