package org.sample.service

import org.sample.exceptions.{BadRequest, NotFound}
import org.sample.models.Game
import org.sample.repository.GamesRepository
import org.scalamock.scalatest.MockFactory
import org.scalatest.FlatSpec

import scala.collection.mutable

class GamesServiceTest extends FlatSpec with MockFactory {

  "GameService.retrieveGames" should "retrieve empty list on initialisation" in {
    val gameRepositoryStub = stub[GamesRepository]
    val gameService = new GamesService(gameRepositoryStub)

    //define stub
    gameRepositoryStub.retrieveGames _ when() returns mutable.TreeMap.empty

    //run system and check expectations
    assert (gameService retrieveGames() equals mutable.TreeMap.empty)
  }

  "GameService.addNewGame" should "add new game on valid request and return its name" in {
    val gameRepositoryMock = mock[GamesRepository]
    val gameService = new GamesService(gameRepositoryMock)

    val newGame = Game("newGame", 2018, 100)

    //define mock
    gameRepositoryMock.addNewGame _ expects newGame returning newGame.name

    //run the system
    assert(gameService addNewGame newGame equals newGame.name)
  }

  "GameService.addNewGame" should "throw exception if repository throws when adding an invalid game" in {
    val gameRepositoryMock = mock[GamesRepository]
    val gameService = new GamesService(gameRepositoryMock)

    val newGame = Game(" ^&#%%@", 2018, 100)

    //define mock
    gameRepositoryMock.addNewGame _ expects newGame throwing new BadRequest("Invalid Id")

    //run the system
    val e = intercept[BadRequest] {
      gameService addNewGame newGame
    }

    assert(e.getMessage equals "Invalid Id")
  }

  "GameService.updateGame" should "delete the old game and add a new one" in {
    val gameRepositoryMock = mock[GamesRepository]
    val gameService = new GamesService(gameRepositoryMock)

    val oldGameName = "oldGameName"
    val newGame = Game("newGame", 2018, 100)

    //define mock
    gameRepositoryMock.deleteGame _ expects oldGameName
    gameRepositoryMock.addNewGame _ expects newGame

    //run the system
    gameService updateGame (newGame, oldGameName)
  }

  "GameService.updateGame" should "throw exception if repository throws when deleting an invalid game" in {
    val gameRepositoryMock = mock[GamesRepository]
    val gameService = new GamesService(gameRepositoryMock)

    val oldGameName = "oldGameName"
    val newGame = Game("newGame", 2018, 100)

    //define mock
    gameRepositoryMock.deleteGame _ expects oldGameName throwing new NotFound("Id not present")

    //run the system
    val e = intercept[NotFound] {
      gameService updateGame(newGame, oldGameName)
    }

    assert(e.getMessage equals "Id not present")
  }

  "GameService.updateGame" should "throw exception if repository throws when adding an invalid game" in {
    val gameRepositoryMock = mock[GamesRepository]
    val gameService = new GamesService(gameRepositoryMock)

    val oldGameName = "oldGameName"
    val newGame = Game("newGame", 2018, 100)

    //define mock
    gameRepositoryMock.deleteGame _ expects oldGameName
    gameRepositoryMock.addNewGame _ expects newGame throwing new BadRequest("Invalid Id")

    //run the system
    val e = intercept[BadRequest] {
      gameService updateGame(newGame, oldGameName)
    }

    assert(e.getMessage equals "Invalid Id")
  }

  "GameService.deleteGame" should "call repository deleteGame with the correct parameter" in {
    val gameRepositoryMock = mock[GamesRepository]
    val gameService = new GamesService(gameRepositoryMock)

    val gameName = "gameName"

    //define mock
    gameRepositoryMock.deleteGame _ expects gameName

    //run the system
    gameService deleteGame gameName
  }

  "GameService.deleteGame" should "throw exception if repository throws when deleting an invalid game" in {
    val gameRepositoryMock = mock[GamesRepository]
    val gameService = new GamesService(gameRepositoryMock)

    val gameName = "gameName"

    //define mock
    gameRepositoryMock.deleteGame _ expects gameName throwing new NotFound("Id not present")

    //run the system
    val e = intercept[NotFound] {
      gameService deleteGame gameName
    }

    assert(e.getMessage equals "Id not present")
  }
}
