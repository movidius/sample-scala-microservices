package org.sample.controller

import io.circe.generic.auto._
import io.circe.syntax._
import org.sample.models.Game
import org.sample.service.GamesService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.{HttpHeaders, HttpStatus, ResponseEntity}
import org.springframework.web.bind.annotation._

@RestController
@RequestMapping(path = Array("/games"))
class GamesController(@Autowired private val gameService: GamesService) {

  @GetMapping
  def getAllGames: String = gameService.retrieveGames().asJson.toString()

  @PostMapping
  def addNewGame(@RequestBody newGame: Game): ResponseEntity[Unit] = {
    gameService addNewGame newGame

    val headers = new HttpHeaders()
    headers add("id", newGame.name)

    new ResponseEntity(headers, HttpStatus.CREATED)
  }

  @PutMapping(path = Array("/{gameId}"))
  def editGame(@RequestBody game: Game, @PathVariable gameId: String): ResponseEntity[Unit] = {
    gameService updateGame(game, gameId)

    new ResponseEntity(HttpStatus.NO_CONTENT)
  }

  @DeleteMapping(path = Array("/{gameId}"))
  def deleteGame(@PathVariable gameId: String): Unit = gameService deleteGame gameId
}
