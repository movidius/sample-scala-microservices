package org.sample.exceptions

import org.springframework.http.HttpStatus.{BAD_REQUEST, NOT_FOUND}
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.client.RestClientException

@ResponseStatus(code = BAD_REQUEST)
final class BadRequest(var message: String) extends RestClientException(message)

@ResponseStatus(code = NOT_FOUND)
final class NotFound(var message: String) extends RestClientException(message)