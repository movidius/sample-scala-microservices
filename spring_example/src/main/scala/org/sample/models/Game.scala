package org.sample.models

case class Game(name: String, releaseYear: Int, price: Int)