package org.sample.service

import org.sample.models.Game
import org.sample.repository.GamesRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import scala.collection.mutable

@Service
class GamesService(@Autowired private val gamesRepository: GamesRepository) {

  def retrieveGames(): mutable.TreeMap[String, Game] = gamesRepository retrieveGames()

  def addNewGame(newGame: Game): String = gamesRepository addNewGame newGame

  def updateGame(newGame: Game, oldGameName: String): Unit = {
    gamesRepository deleteGame oldGameName
    gamesRepository addNewGame newGame
  }

  def deleteGame(gameName: String): Unit = gamesRepository deleteGame gameName
}
