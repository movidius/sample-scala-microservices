package org.sample.repository

import org.sample.exceptions.{BadRequest, NotFound}
import org.sample.models.Game
import org.springframework.stereotype.Repository

import scala.collection.mutable

@Repository
class GamesRepository {
  private val games: mutable.TreeMap[String, Game] = mutable.TreeMap.empty

  def retrieveGames(): mutable.TreeMap[String, Game] = games

  def addNewGame(newGame: Game): String = {
    validateId(newGame.name)

    if (games contains newGame.name) {
      throw new BadRequest("Id already present. Try editing the object instead")
    }

    games put(newGame.name, newGame)

    newGame.name
  }

  def deleteGame(gameName: String): Unit = {
    if (!(games contains gameName)) {
      throw new NotFound("Id not present")
    }

    games remove gameName
  }

  private def validateId(gameName: String): Unit = {
    if (!(gameName matches "[0-9a-zA-Z]*")) {
      throw new BadRequest("Invalid Id")
    }
  }
}