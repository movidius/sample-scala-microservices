# Sample Scala microservices

All the examples have the same base functionality:   
_ Retrieving the list of games    
_ Adding games to the list    
_ Updating specific games' information    
_ Deleting specific games    

**API endpoints:  **    
   
   
```
GET: /games  
    produces:  
      - "application/json"  
	responses:  
		200:  
			description: "Ok"  
			schema:  
				TreeMap[String, Game] --- The name of the game is used as key, hence the String  
		  
POST: /games  
	consumes:  
	  - "application/json"  
	produces:  
      - "application/json"  
	parameters:  
		- name: "payload"  
          in: "body"  
          description: "The game JSON payload"  
          required: true  
          schema:  
            Game  
	responses:  
		201:  
			description: "Created"  
			headers:  
				id:  
	            	type: "string"  
 		            description: "Unique identifier for the game created"  
		400:  
			description: "Bad request"  
			schema:  
				BadRequest --- custom exception  
	  
PUT: /games/{game-id}  
	consumes:  
	  - "application/json"  
	produces:  
      - "application/json"  
	parameters:  
		- name: "game-id"  
          in: "path"  
          description: "The unique game id"  
          required: true  
          type: "string"  
		- name: "payload"  
          in: "body"  
          description: "The game JSON payload"  
          required: true  
          schema:  
            Game  
	responses:  
		204:  
			description: "No content"  
		400:  
			description: "Bad request"  
			schema:  
				BadRequest --- custom exception  
		404:  
			description: "Not found"  
			schema:  
				NotFound --- custom exception  
          
DELETE: /games/{game-id}  
	produces:  
      - "application/json"  
	parameters:  
		- name: "game-id"  
          in: "path"  
          description: "The unique game id"  
          required: true  
          type: "string"  
	responses:  
		204:  
			description: "No content"  
		404:  
			description: "Not found"  
			schema:  
				NotFound --- custom exception  
```